
var ProgressCountdown = function($el, options) {
	var _this = this,
		defaults = {
		total: 100,
		start: 0,
		step: 1,
		getText: function(nbr) {
			return nbr;
		}
	},
		$progress = $el.find('.countdown--progress'),
		$progress_inner = $progress.find('.countdown--progress--inner'),
		$left = $el.find('.countdown--seconds');

	this.options = $.extend({}, defaults, options);

	this.current_progress = this.options.start;

	progress(false, true);

	this.progress = progress;

	this.reset = function() {
		progress(this.options.start, true);
	};

	function progress(current, start) {
		if(undefined !== current && false !== current) {
			_this.current_progress = current;
		}
		else if(!start) {
			_this.current_progress += _this.options.step;
		}
		var text = _this.options.getText(_this.current_progress);

//					console.log(_this.options.total, _this.current_progress, 100 / (_this.options.total / _this.current_progress) + '%');
		$progress_inner.width(100 / (_this.options.total / _this.current_progress) + '%');
		$left.text(text);
	}
};